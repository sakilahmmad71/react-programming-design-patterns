import React from 'react';

interface ITitlePropsTypes {
	children: React.ReactNode;
}

const Title: React.FunctionComponent<ITitlePropsTypes> = ({ children = null }) => {
	return <h2>{children}</h2>;
};

export default React.memo(Title);
