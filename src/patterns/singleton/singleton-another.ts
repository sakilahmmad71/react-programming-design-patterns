let counter: number = 0;

export class Singleton {
	static instance: object;

	constructor() {
		if (Singleton.instance) {
			throw new Error('Cannot create another instance in singleton pattern!');
		}

		Singleton.instance = this;
	}

	getInstance() {
		return this;
	}

	getCount() {
		return counter;
	}

	increment() {
		return ++counter;
	}

	decrement() {
		return --counter;
	}
}

const singleton = Object.freeze(new Singleton());

export default singleton;
