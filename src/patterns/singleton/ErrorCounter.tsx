import React from 'react';
import { Singleton } from './singleton';

const ErrorCounter: React.FunctionComponent = () => {
	const counter = new Singleton();
	const [state, stateSet] = React.useState(counter.getCount());

	const handleIncrement = () => {
		counter.increment();
		stateSet(counter.getCount());
	};

	const handleDecrement = () => {
		counter.decrement();
		stateSet(counter.getCount());
	};

	return (
		<div>
			<h1>{state}</h1>

			<button type='button' onClick={handleIncrement}>
				Increase
			</button>
			<button type='button' onClick={handleDecrement}>
				Decrease
			</button>
		</div>
	);
};

export default ErrorCounter;
