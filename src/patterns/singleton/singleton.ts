/**
 * Singleton design parrent can be instanciate only once
 * Redux or other state management library works the same way
 * What if we make more than one instance it will throw error as boundary error
 * Singleton instance should be same like singleton1.getInstance() === singleton2.getInstance() => true
 * Central the data and logic between components so that we get always the updated data wheather implementatio in different files
 * Singletons are actually considered an anti-pattern, and can (or.. should) be avoided in JavaScript
 * The government is an excellent example of the Singleton pattern. A country can have only one official government
 * A Facade class can often be transformed into a Singleton since a single facade object is sufficient in most cases
 * Abstract Factories, Builders and Prototypes can all be implemented as Singletons.
 */

let instance: object;
let counter = 0;

export class Singleton {
	constructor() {
		if (instance) {
			throw new Error('Cannot create another instance in singleton pattern!');
		}

		instance = this;
	}

	getInstance() {
		return this;
	}

	getCount() {
		return counter;
	}

	increment() {
		return ++counter;
	}

	decrement() {
		return --counter;
	}
}

const singleton = Object.freeze(new Singleton());

export default singleton;
