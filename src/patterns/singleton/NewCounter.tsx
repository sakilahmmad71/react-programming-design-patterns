import React from 'react';
import singletonCounter from './singleton';

const NewCounter: React.FunctionComponent = () => {
	const counter = singletonCounter;
	const [state, stateSet] = React.useState(counter.getCount());

	const handleIncrement = () => {
		counter.increment();
		stateSet(counter.getCount());
	};

	const handleDecrement = () => {
		counter.decrement();
		stateSet(counter.getCount());
	};

	return (
		<div>
			<h1>{state}</h1>

			<button type='button' onClick={handleIncrement}>
				Increase
			</button>
			<button type='button' onClick={handleDecrement}>
				Decrease
			</button>
		</div>
	);
};

export default NewCounter;
