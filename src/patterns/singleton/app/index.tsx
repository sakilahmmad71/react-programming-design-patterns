import React, { useCallback } from 'react';
import counter from './Counter';
import Title from '../../../components/Title';

const SingletonApp: React.FunctionComponent = () => {
	const [count, countSet] = React.useState(counter.getCount());

	const handleIncrement = useCallback(() => {
		console.log('Re-rendering increment');
		countSet(counter.increment());
	}, []);

	const handleDecrement = useCallback(() => {
		console.log('Re-rendering decrement');
		countSet(counter.decrement());
	}, []);

	return (
		<>
			<Title>Singleton Pattern</Title>

			<p>
				<strong>{count}</strong>
			</p>

			<button type='button' onClick={handleIncrement}>
				+ Increment
			</button>

			<button type='button' onClick={handleDecrement}>
				- Decrement
			</button>
		</>
	);
};

export default SingletonApp;
