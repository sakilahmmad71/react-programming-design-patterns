let instance: object | boolean;
let count: number = 0;

class Counter {
	constructor() {
		if (instance) throw new Error('Cannot instanciate twice!');
		instance = this;
	}

	getInstance() {
		return this;
	}

	getCount() {
		return count;
	}

	increment() {
		return ++count;
	}

	decrement() {
		return --count;
	}
}

const counter = Object.freeze(new Counter());

export default counter;
