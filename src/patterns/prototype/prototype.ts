/**
 * The prototype pattern allows us to easily let objects access and inherit properties from other objects.
 * Since the prototype chain allows us to access properties that aren't directly defined on the object itself,
 * we can avoid duplication of methods and properties, thus reducing the amount of memory used
 */

export class Dog {
	name: string;

	constructor(name: string) {
		this.name = name;
	}

	bark() {
		console.log('Woof!');
	}
}

class SuperDog extends Dog {
	// eslint-disable-next-line @typescript-eslint/no-useless-constructor
	constructor(name: string) {
		super(name);
	}

	fly() {
		console.log(`Flying!`);
	}
}

const dog1 = new SuperDog('Daisy');
dog1.bark();
dog1.fly();

const dog = {
	bark() {
		console.log(`Woof!`);
	},
};

const pet1 = Object.create(dog);

pet1.bark(); // Woof!
console.log('Direct properties on pet1: ', Object.keys(pet1));
console.log("Properties on pet1's prototype: ", Object.keys(pet1.__proto__));
