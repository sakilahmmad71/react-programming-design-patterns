/**
 * We've used provider pattern previously so make sure follow those code examples for future usage
 * overusing the Provider pattern can result in performance issues. All components that consume the context re-render on each state change
 * To make sure that components aren't consuming providers that contain unnecessarily values which may update, you can create several providers for each separate usecase
 */

import * as React from 'react';

type Action = { type: 'increment' } | { type: 'decrement' };
type Dispatch = (action: Action) => void;
type State = { count: number };
type CountProviderProps = { children: React.ReactNode };

const CountStateContext = React.createContext<{ state: State; dispatch: Dispatch } | undefined>(
	undefined
);

function countReducer(state: State, action: Action) {
	switch (action.type) {
		case 'increment': {
			return { count: state.count + 1 };
		}
		default: {
			throw new Error(`Unhandled action type: ${action.type}`);
		}
	}
}

function CountProvider({ children }: CountProviderProps) {
	const [state, dispatch] = React.useReducer(countReducer, { count: 0 });
	// NOTE: you *might* need to memoize this value
	// Learn more in http://kcd.im/optimize-context
	const value = { state, dispatch };
	return <CountStateContext.Provider value={value}>{children}</CountStateContext.Provider>;
}

function useCount() {
	const context = React.useContext(CountStateContext);
	if (context === undefined) {
		throw new Error('useCount must be used within a CountProvider');
	}
	return context;
}

export { CountProvider, useCount };
