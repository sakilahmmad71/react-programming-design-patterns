import React, { useMemo } from 'react';

type Action = { type: 'increment' } | { type: 'decrement' } | { type: '' };
type Dispatch = (action: Action) => void;
type State = { count: number };

type InitialState = { state: State; dispatch: Dispatch };
const CounterContext = React.createContext<InitialState | undefined>(undefined);

const countReducer = (state: State, action: Action) => {
	switch (action.type) {
		case 'increment': {
			return { count: state.count + 1 };
		}

		case 'decrement': {
			return { count: state.count - 1 };
		}

		default: {
			throw new Error(`Unhandled action type: ${action.type}`);
		}
	}
};

type CountProviderProps = { children: React.ReactNode };

const CountProvider = ({ children }: CountProviderProps) => {
	const [state, dispatch] = React.useReducer(countReducer, {
		count: 0,
	});

	const value = useMemo(() => ({ state, dispatch }), [state]);

	return <CounterContext.Provider value={value}>{children}</CounterContext.Provider>;
};

const useCounter = () => {
	const context = React.useContext(CounterContext);

	if (context === undefined) {
		throw new Error('useCounter must be used within a CountProvider');
	}

	return context;
};

export { CountProvider, useCounter };
