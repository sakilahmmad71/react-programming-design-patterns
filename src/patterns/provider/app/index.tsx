import React from 'react';
import Title from '../../../components/Title';
import { useCounter } from './Provider';

const ProviderApp = () => {
	const { state, dispatch } = useCounter();

	const handleIncrement = () => dispatch({ type: 'increment' });

	const handleDecrement = () => dispatch({ type: 'decrement' });

	return (
		<>
			<Title>Provider Pattern</Title>

			<p>
				<strong>{state.count}</strong>
			</p>

			<button type='button' onClick={handleIncrement}>
				+ Increment
			</button>

			<button type='button' onClick={handleDecrement}>
				- Decrement
			</button>
		</>
	);
};

export default ProviderApp;
