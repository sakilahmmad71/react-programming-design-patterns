import Title from "../../../components/Title";
import useHover from "./useHover";
import withLoader from "./withLoader";

const ListOfImages = ({ data = [] }) => {
  const [ref, hovering] = useHover();

  return (
    <>
      <Title>Hooks Pattern</Title>

      <div>
        {hovering && <p>Hovering!</p>}
      </div>

      <div ref={ref}>
        {data.message.map((item) => <img key={item} src={item} height={200} width={200} alt={`Item ${item}`} />)}
      </div>
    </>
  )
}

export default withLoader(ListOfImages, "https://dog.ceo/api/breed/labrador/images/random/6");