import { useEffect, useRef, useState } from 'react'

const useHover = () => {
  const [hovering, hoveringSet] = useState(false);
  const ref = useRef(null)

  const handleEnterHover = () => hoveringSet(true);
  const handleLeaveHover = () => hoveringSet(false);

  useEffect(() => {
    const node = ref.current;

    if (node) {
      node.addEventListener("mouseover", handleEnterHover);
      node.addEventListener("mouseout", handleLeaveHover);

      return () => {
        node.removeEventListener("mouseover", handleEnterHover);
        node.removeEventListener("mouseout", handleLeaveHover);
      };
    }
  }, []);

  return [ref, hovering]
}

export default useHover;