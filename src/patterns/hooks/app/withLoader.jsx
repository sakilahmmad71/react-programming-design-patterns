import { useEffect, useState } from "react";

const withLoader = (Component = null, url = '') => (props) => {
  const [data, dataSet] = useState(null);

  useEffect(() => {
    const getData = async () => {
      const res = await fetch(url);
      const result = await res.json();
      dataSet(result);
    }

    getData();
  }, []);

  if (!data) {
    return <div>Loading...</div>
  }

  return <Component {...props} data={data} />
}

export default withLoader;