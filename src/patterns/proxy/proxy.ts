/**
 * Can manipulate object as we needed though we're having an extra layer top of original object
 */

interface Person {
	name: string;
	age: number;
	city: string;
}

const person: Person = {
	name: 'Shakil Ahmed',
	age: 23,
	city: 'Dhaka',
};

export const personProxy = new Proxy(person, {
	get: (obj: Person, prop: string) => {
		console.log(`The value of ${prop} is ${Reflect.get(obj, prop)}`);
	},

	set: (obj: Person, prop: string, value: string | number) => {
		console.log(`Changed ${prop} from ${Reflect.get(obj, prop)} to ${value}`);
		Reflect.get(obj, prop, value);
		return true;
	},
});
