export interface IPerson {
	name: string;
	age: number;
	city: string;
	country: string;
}

const person: IPerson = {
	name: 'Shakil Ahmed',
	age: 22,
	city: 'Dhaka',
	country: 'Bangladesh',
};

export const personProxy = new Proxy(person, {
	get: (target: IPerson, prop: string) => {
		return `Person ${prop} is ${Reflect.get(target, prop)}`;
	},

	set: (target: IPerson, prop: string, value: string | number) => {
		Reflect.set(target, prop, value.toString());
		return true;
	},
});
