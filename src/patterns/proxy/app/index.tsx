import React from 'react';
import Title from '../../../components/Title';
import { personProxy } from './proxy';

const ProxyApp: React.FunctionComponent = () => {
	personProxy.age = 23;

	return (
		<>
			<Title>Proxy Pattern</Title>

			<p>Name : {personProxy.name}</p>
			<p>Age : {personProxy.age}</p>
			<p>City : {personProxy.city}</p>
			<p>Country : {personProxy.country}</p>
		</>
	);
};

export default ProxyApp;
