import React from 'react';

type CalculatePropsTypes = {
	type: string;
	value: number;
};

const Calculate: React.FunctionComponent<CalculatePropsTypes> = ({
	type = 'kelvin',
	value = 0,
}) => {
	let temperature = '';

	if (type === 'kelvin') {
		temperature = value ? `${value + 273.15} K` : '273.15 K';
	}

	if (type === 'farenheit') {
		temperature = value ? `${(value * 9) / 5 + 32} F` : '32 F';
	}
	return <div>Temperature is {temperature}</div>;
};

export default Calculate;
