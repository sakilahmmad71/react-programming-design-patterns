import React from 'react';
import Title from '../../../components/Title';
import Calculate from './Calculate';
import InputChildrens from './InputChildrens';
import InputProps from './InputProps';

const RenderPropsApp: React.FunctionComponent = () => {
	return (
		<div>
			<Title>Render Props and Childrens Pattern</Title>

			{/* Render children pattern */}
			<InputChildrens>
				{(value: number) => (
					<>
						<Calculate type='kelvin' value={value} />
						<Calculate type='farenheit' value={value} />
					</>
				)}
			</InputChildrens>

			{/* Render props pattern */}
			<InputProps
				render={(value: number) => (
					<>
						<Calculate type='kelvin' value={value} />
						<Calculate type='farenheit' value={value} />
					</>
				)}
			/>
		</div>
	);
};

export default RenderPropsApp;
