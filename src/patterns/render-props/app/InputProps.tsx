import React, { useState } from 'react';

type InputPropsTypes = {
	render: any;
};

const InputProps: React.FunctionComponent<InputPropsTypes> = ({ render = null }) => {
	const [value, valueSet] = useState<number | undefined>(undefined);

	const handleChangeValue = (e: React.ChangeEvent<HTMLInputElement>) =>
		valueSet(parseInt(e.target.value));

	return (
		<div>
			<input type='number' value={value} onChange={handleChangeValue} />
			{render(value)}
		</div>
	);
};

export default InputProps;
