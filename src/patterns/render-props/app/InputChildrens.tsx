import React, { useState } from 'react';

type InputChildrensTypes = {
	children: any;
};

const InputChildrens: React.FunctionComponent<InputChildrensTypes> = ({ children = null }) => {
	const [value, valueSet] = useState<number | undefined>(undefined);

	const handleChangeValue = (e: React.ChangeEvent<HTMLInputElement>) =>
		valueSet(parseInt(e.target.value));

	console.log(typeof children);

	return (
		<div>
			<input type='number' value={value} onChange={handleChangeValue} />
			{children(value)}
		</div>
	);
};

export default InputChildrens;
