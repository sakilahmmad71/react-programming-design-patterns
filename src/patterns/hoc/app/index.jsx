import Title from "../../../components/Title";
import withHover from "./withHover";
import withLoader from "./withLoader";

const ListOfImages = ({ data = [], hovering = false, ...props }) => {
  return (
    <>
      <Title>HOC Pattern</Title>

      <div>
        {hovering && <p>Hovering!</p>}
      </div>

      <div>
        {data.message.map((item) => <img {...props} key={item} src={item} height={200} width={200} alt={`Item ${item}`} />)}
      </div>
    </>
  )
}

export default withHover(withLoader(ListOfImages, "https://dog.ceo/api/breed/labrador/images/random/6"));