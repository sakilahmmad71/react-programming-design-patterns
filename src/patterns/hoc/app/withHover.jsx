import { useState } from 'react'

const withHover = (Element = null) => (props) => {
  const [hovering, hoveringSet] = useState(false);

  return (
    <Element
      {...props}
      hovering={hovering}
      onMouseEnter={() => hoveringSet(true)}
      onMouseLeave={() => hoveringSet(false)}
    />
  )
}

export default withHover;