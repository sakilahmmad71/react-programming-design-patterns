import React, { useState } from 'react';

type ChildrenPropsTypes = {
	children: React.ReactNode;
};

const List: React.FunctionComponent<ChildrenPropsTypes> = ({ children }) => {
	const [count, countSet] = useState(0);

	return (
		<div>
			{React.Children.map(children, (child) =>
				React.cloneElement(child as React.ReactElement<any>, { count, countSet })
			)}
		</div>
	);
};

export default List;
