import React from 'react';
import Item from './Item';
import List from './List';

const ListItems = () => {
	return (
		<List>
			<Item />
			<Item />
			<Item />
			<Item />
		</List>
	);
};

export default ListItems;
