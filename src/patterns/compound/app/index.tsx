import React from 'react';
import Title from '../../../components/Title';
import ListItems from '../ListItems';

const CompoundApp: React.FunctionComponent = () => {
	return (
		<div>
			<Title>Compound Component Pattern</Title>
			<p>Compound Component</p>
			<ListItems />
		</div>
	);
};

export default CompoundApp;
