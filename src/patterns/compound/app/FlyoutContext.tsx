import React, { createContext, useContext, useMemo, useState } from 'react';

interface IFlyoutContextType {
	open: boolean;
	toggle?: React.Dispatch<React.SetStateAction<boolean>>;
}

const initialState = { open: false };

export const FlyoutContext = createContext<IFlyoutContextType>(initialState);

type ChildrenPropsTypes = {
	children: React.ReactNode;
};

const Flyout = (props: ChildrenPropsTypes) => {
	const [open, toggle] = useState<boolean>(false);

	const value = useMemo(() => ({ open, toggle }), [open]);

	return <FlyoutContext.Provider value={value}>{props.children}</FlyoutContext.Provider>;
};

const Toggle = () => {
	const { open, toggle } = useContext(FlyoutContext);

	return <div onClick={() => toggle}>{/* <Icon /> */}</div>;
};

const List = ({ children }: ChildrenPropsTypes) => {
	const { open } = useContext(FlyoutContext);
	return open && <ul>{children}</ul>;
};

const Item = ({ children }: ChildrenPropsTypes) => {
	return <li>{children}</li>;
};

Flyout.Toggle = Toggle;
Flyout.List = List;
Flyout.Item = Item;

// export function FlyOut(props) {
//   const [open, toggle] = React.useState(false);

//   return (
//     <div>
//       {React.Children.map(props.children, child =>
//         React.cloneElement(child, { open, toggle })
//       )}
//     </div>
//   );
// }
