/**
 * By using this hook, we no longer need the container component to fetch the data, and send this to the presentational component
 * we still separated the application logic from the view
 * testing presentational components is easy, as they are usually pure functions
 */

import React, { useState, useEffect } from 'react';

const useDogImages = () => {
	const [dogs, dogsSet] = useState<string[]>([]);

	const fetchDogs = (url: string): void => {
		/**
		 * Get list of dogs by calling apis
		 * make sure all dogs data available to dogsSet function
		 */
		dogsSet(['Matt', 'Tigs']);
	};

	useEffect(() => fetchDogs('/dogs'), []);

	return dogs;
};

const HooksWay: React.FunctionComponent = () => {
	const dogs = useDogImages();

	return (
		<>
			{dogs.map((dog, index) => (
				<p key={index}>{dog}</p>
			))}
		</>
	);
};

export default HooksWay;
