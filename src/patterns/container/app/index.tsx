import React from 'react';
import Title from '../../../components/Title';
import Presentation from './Presentation';
import useImages from './useImages';

// const countryCodeToFlag = (countryCode: string) => {
// 	return typeof String.fromCodePoint !== 'undefined'
// 		? countryCode
// 				.toUpperCase()
// 				.replace(/./g, (char) => String.fromCodePoint(char.charCodeAt(0) + 127397))
// 		: countryCode;
// };

const ContainerApp = () => {
	const dogs = useImages();

	return (
		<div>
			<Title>Container / Presentation Pattern</Title>

			{dogs.map((dog) => (
				<Presentation key={dog} dog={dog} />
			))}
		</div>
	);
};

export default ContainerApp;
