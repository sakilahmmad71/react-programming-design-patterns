import React from 'react';

interface IPresentationPropsTypes {
	dog: string;
}

const Presentation: React.FunctionComponent<IPresentationPropsTypes> = ({ dog = '' }) => {
	return <img src={dog} alt={`dog}`} height={200} width={200} />;
};

export default Presentation;
