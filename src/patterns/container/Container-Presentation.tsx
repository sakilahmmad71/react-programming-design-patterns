/**
 * Presentational components receive their data from container components
 * Presentational components are usually stateless: they do not contain their own React state, unless they need a state for UI purposes
 * Fetching the dog images deals with application logic, whereas displaying the images only deals with the view
 * primary function is to simply display the data it receives the way we want them to, including styles, without modifying that data
 *
 */

import React, { useEffect, useState } from 'react';

const Container: React.FunctionComponent = () => {
	const [dogs, dogsSet] = useState<string[]>([]);

	const fetchDogs = (url: string): void => {
		/**
		 * Get list of dogs by calling apis
		 * make sure all dogs data available to dogsSet function
		 */
		dogsSet(['Matt', 'Tigs']);
	};

	useEffect(() => fetchDogs('/dogs'), []);

	return <Presentation dogs={dogs} />;
};

interface IPresentationPropsTypes {
	dogs: string[];
}

const Presentation: React.FunctionComponent<IPresentationPropsTypes> = ({ dogs = [] }) => {
	return (
		<>
			{dogs.map((dog, index) => (
				<p key={index}>{dog}</p>
			))}
		</>
	);
};

export default Container;
