import React from 'react';
import ContainerApp from './patterns/container/app';
import ProviderApp from './patterns/provider/app';
import { CountProvider } from './patterns/provider/app/Provider';
import ProxyApp from './patterns/proxy/app';
import SingletonApp from './patterns/singleton/app';
import HocApp from './patterns/hoc/app';
import HooksApp from './patterns/hooks/app';
import RenderPropsApp from './patterns/render-props/app';
import CompoundApp from './patterns/compound/app';

const App: React.FunctionComponent = () => {
	return (
		<>
			<h1>Design Patterns!</h1>
			<SingletonApp />
			<ProxyApp />
			<CountProvider>
				<ProviderApp />
			</CountProvider>
			<ContainerApp />
			<HocApp />
			<HooksApp />
			<RenderPropsApp />
			<CompoundApp />
		</>
	);
};

export default App;
